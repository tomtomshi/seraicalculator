## Quick Summary
Realize the business requirements of Credit Assessment Calculator riding on the technical requirements guide line.

For demonstration purpose and time constraint, exception handling etc have not been implemented.


## Technology Used

Java 11

Spring Boot 2.2.6

Swagger2 2.7.0

Cucumber 4.2.0

Maven 4.0.0

Docker 19.03.8

## How to Start

### Get the source from bitbucket:

```
git clone https://tomtomshi@bitbucket.org/tomtomshi/seraicalculator.git
```

### Run the Credit Assessment Calculator by:

#### Local Env Build and Run

Use the dev tool (e.g. VS Code) or command line to open the Maven project

- Install and Build:
```
mvn clean install
mvn clean package
```

- Run it:

```
./mvnw spring-boot:run
```

#### Docker Build and Run

In the project folder 

- Build the docker:

```
docker build -t serai/calculator . 
```

- Run the docker:

```
docker run --name SeraiCreditScoreCalculator -p 8080:8080 serai/calculator
```

### Try it

Credit Assessment Calculator Restful API 

```
http://localhost:8080/swagger-ui.html
```


As BDD perspective, provides readable outlineCreditAssess.feature so that the user can verify the score result and also can add additional test

```
Feature: Credit Assessment Calculator
    As a user
    I want to get the credit assess socre
    
    Scenario Outline: Calculate Credit Assessment Score by <companyType> & No. of Employee=<numberOfEmployee> & Year Operated==<yearOperated>

        Given I have the Calculator
        When I pass <companyType> and <numberOfEmployee> and <yearOperated>
        Then The credit score should be <result>

        Examples:
        | companyType   | numberOfEmployee | yearOperated | result |
        | SOLE          | 0                | 0            | 12     |
        | SOLE          | 1                | 3            | 12     |
        | SOLE          | 10               | 3            | 32     |
        | SOLE          | 21               | 5            | 110    |
        | LTD           | 0                | 0            | 63     |
        | LTD           | 1                | 3            | 63     |
        | LTD           | 21               | 5            | 161    |
        | LTD           | 100              | 20           | 192    |        
        | PARTNERSHIP   | 0                | 0            | 75     |
        | PARTNERSHIP   | 1                | 3            | 75     |
        | PARTNERSHIP   | 20               | 10           | 166    |
        | PARTNERSHIP   | 100              | 20           | 204    |        
        | OTHERS        | 0                | 0            | 0      |        
        | OTHERS        | 1                | 3            | 0      |        
        | OTHERS        | 7                | 5            | 48     |        
        | OTHERS        | 100              | 20           | 129    |  
```