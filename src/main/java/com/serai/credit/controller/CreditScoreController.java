package com.serai.credit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.WebDataBinder;



import com.serai.credit.model.ScoreCriteria;
import com.serai.credit.model.CompanyType;
import com.serai.credit.model.CompanyTypeConverter;
import com.serai.credit.service.CreditScoreService;

@RestController
public class CreditScoreController {
        
        @Autowired
        private CreditScoreService creditScoreService;
        private ScoreCriteria scoreCriteria;
        
        @GetMapping("/calculateCreditScore")
        public Integer calculateCreditScore(@RequestParam("companyType") CompanyType companyType, 
                                            @RequestParam(required=true) int noOfEmployee,
                                            @RequestParam(required=true) int yearOperated) {

            scoreCriteria = new ScoreCriteria(companyType, noOfEmployee, yearOperated);
        
            return creditScoreService.calculate(scoreCriteria);        
        }

        @InitBinder
        public void initBinder(final WebDataBinder webdataBinder) {
            webdataBinder.registerCustomEditor(CompanyType.class, new CompanyTypeConverter());
        }

    }