package com.serai.credit.service;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.serai.credit.model.ScoreCriteria;
import com.serai.credit.model.CompanyType;

@Service
@Validated
public class CreditScoreService {
    
    //Company Type
    private static final int SCORE_COMTYPE_SOLE = 12;
    private static final int SCORE_COMTYPE_LTD = 63;
    private static final int SCORE_COMTYPE_PARTNERSHIP = 75;
    private static final int SCORE_COMTYPE_OTHERS = 0;

    //Number of Employee
    private static final int SCORE_EMPLOY_1_TO_5 = 0;
    private static final int SCORE_EMPLOY_6_TO_10 = 20;
    private static final int SCORE_EMPLOY_11_TO_15 = 32;
    private static final int SCORE_EMPLOY_16_TO_20 = 55;
    private static final int SCORE_EMPLOY_GREATER_20 = 70; 

    //Time in Business (Year)
    private static final int SCORE_YEAR_OPERATED_0_TO_3 = 0;
    private static final int SCORE_YEAR_OPERATED_4_TO_9 = 28;
    private static final int SCORE_YEAR_OPERATED_10_TO_15 = 36;
    private static final int SCORE_YEAR_OPERATED_GREATER_16 = 59;

    public static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }  

    public Integer calculate(@Validated ScoreCriteria scoreCriteria) {

        int scoreComType = 0;
        int scoreNoOfEmploy = 0;
        int scoreYearOperated = 0; 

        CompanyType companyType = scoreCriteria.getCompanyType();
        int noOfEmploy = scoreCriteria.getNoOfEmployee();
        int yearOperated = scoreCriteria.getYearOperated();

        //Score of Company Type
        if (companyType == CompanyType.SOLE)
            scoreComType = SCORE_COMTYPE_SOLE;

        else if (companyType == CompanyType.LTD)
                scoreComType = SCORE_COMTYPE_LTD;
                
        else if (companyType == CompanyType.PARTNERSHIP)
                scoreComType = SCORE_COMTYPE_PARTNERSHIP;

        else if (companyType == CompanyType.OTHERS)
                scoreComType = SCORE_COMTYPE_OTHERS;
        
                
        //Score of No of Employee                
        if (isBetween(noOfEmploy, 0, 5)) 
            scoreNoOfEmploy = SCORE_EMPLOY_1_TO_5;

        else if (isBetween(noOfEmploy,6, 10))      
            scoreNoOfEmploy = SCORE_EMPLOY_6_TO_10;
                
        else if (isBetween(noOfEmploy, 11, 15))      
            scoreNoOfEmploy = SCORE_EMPLOY_11_TO_15;
            
        else if (isBetween(noOfEmploy, 16, 20))                  
            scoreNoOfEmploy = SCORE_EMPLOY_16_TO_20;

        else if (noOfEmploy > 20)                  
            scoreNoOfEmploy = SCORE_EMPLOY_GREATER_20;

            
        //Score of Time in Business
        if (isBetween(yearOperated, 0, 3)) 
            scoreYearOperated = SCORE_YEAR_OPERATED_0_TO_3;

        else if (isBetween(yearOperated, 4, 9))      
            scoreYearOperated = SCORE_YEAR_OPERATED_4_TO_9;

        else if (isBetween(yearOperated, 10, 15))      
            scoreYearOperated = SCORE_YEAR_OPERATED_10_TO_15;

        else if (yearOperated > 16)
            scoreYearOperated = SCORE_YEAR_OPERATED_GREATER_16;


        //Sum up to Total Score
        int totalScore = scoreComType + scoreNoOfEmploy + scoreYearOperated;
        Integer TotalScore = Integer.valueOf(totalScore);
        
        return TotalScore;
    }

}