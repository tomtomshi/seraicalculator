package com.serai.credit.model;

import java.util.Arrays;

public enum CompanyType { 

    SOLE("sole"), LTD("ltd"), PARTNERSHIP("partnership"), OTHERS("others");

    private String value;

    private CompanyType(String value) {
        this.value = value;

    }

    public static CompanyType fromValue(String value) {
		for (CompanyType companyType : values()) {
			if (companyType.value.equalsIgnoreCase(value)) {
				return companyType;
			}
		}
		throw new IllegalArgumentException(
				"Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
	} 

}