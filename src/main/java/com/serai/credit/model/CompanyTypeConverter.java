package com.serai.credit.model;

import java.beans.PropertyEditorSupport;

public class CompanyTypeConverter extends PropertyEditorSupport{

	 public void setAsText(final String text) throws IllegalArgumentException {
	        setValue(CompanyType.fromValue(text));
	    }

}