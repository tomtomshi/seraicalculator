package com.serai.credit.model;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;

@Getter
@Setter
@AllArgsConstructor
public class ScoreCriteria {
    
    @NotNull
    private CompanyType companyType;

    @NotNull
    private int noOfEmployee;

    @NotNull
    private int yearOperated;

}

