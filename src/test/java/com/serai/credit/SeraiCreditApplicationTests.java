package com.serai.credit;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.serai.credit.model.ScoreCriteria;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
class SeraiCreditApplicationTests {

	@Autowired
    private TestRestTemplate restTemplate;

	@LocalServerPort
    int randomServerPort;

	@Test
	void contextLoads() {
	}

	@Test
	public void testCalculatorSuccessSoleProprietorship() throws URISyntaxException {

		restTemplate = new TestRestTemplate();

		final StringBuilder baseUrl = new StringBuilder("http://localhost:" + randomServerPort);
			baseUrl.append("/calculateCreditScore?" +
							"companyType="  + "sole" + 
							"&noOfEmployee=" + "3" + 
							"&yearOperated=" + "5");
		
		URI uri = new URI(baseUrl.toString());

		HttpHeaders headers = new HttpHeaders();
		headers.set("X-COM-PERSIST", "true");  

		HttpEntity<ScoreCriteria> requestEntity = new HttpEntity<>(null, headers);
		
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);

		//Verify request succeed
		Assert.assertEquals(200, result.getStatusCodeValue());
		Assert.assertEquals(40, Integer.parseInt(result.getBody()));

	}

	@Test
	public void testCalculatorSuccessLimited() throws URISyntaxException {

		restTemplate = new TestRestTemplate();

		final StringBuilder baseUrl = new StringBuilder("http://localhost:" + randomServerPort);
			baseUrl.append("/calculateCreditScore?" +
							"companyType="  + "ltd" + 
							"&noOfEmployee=" + "50" + 
							"&yearOperated=" + "10");
		
		URI uri = new URI(baseUrl.toString());

		HttpHeaders headers = new HttpHeaders();
		headers.set("X-COM-PERSIST", "true");  

		HttpEntity<ScoreCriteria> requestEntity = new HttpEntity<>(null, headers);
		
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);

		//Verify request succeed
		Assert.assertEquals(200, result.getStatusCodeValue());
		Assert.assertEquals(169, Integer.parseInt(result.getBody()));

	}

	@Test
	public void testCalculatorSuccessPartnership() throws URISyntaxException {

		restTemplate = new TestRestTemplate();

		final StringBuilder baseUrl = new StringBuilder("http://localhost:" + randomServerPort);
			baseUrl.append("/calculateCreditScore?" +
							"companyType="  + "partnership" + 
							"&noOfEmployee=" + "50" + 
							"&yearOperated=" + "10");
		
		URI uri = new URI(baseUrl.toString());

		HttpHeaders headers = new HttpHeaders();
		headers.set("X-COM-PERSIST", "true");  

		HttpEntity<ScoreCriteria> requestEntity = new HttpEntity<>(null, headers);
		
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);

		//Verify request success
		Assert.assertEquals(200, result.getStatusCodeValue());
		Assert.assertEquals(181, Integer.parseInt(result.getBody()));

	}

}
