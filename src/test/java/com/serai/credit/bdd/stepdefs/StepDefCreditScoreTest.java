package com.serai.credit.bdd.stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import org.junit.Assert;
import org.hamcrest.Matchers;

import com.serai.credit.service.CreditScoreService;
import com.serai.credit.model.ScoreCriteria;
import com.serai.credit.model.CompanyType;

public class StepDefCreditScoreTest {
    
    private CreditScoreService creditScoreService;

    private int scoreTotal;


    @Given("^I have the Calculator$")
    public void initializeCalculator() throws Throwable {
        creditScoreService = new CreditScoreService();
    }

    @When("^I pass (-?.*) and (-?\\d+) and (-?\\d+)$")
    public void testCalculate(CompanyType companyType, int numberOfEmployee, int yearOperated) {
        
        ScoreCriteria scoreCriteria = new ScoreCriteria(companyType, numberOfEmployee, yearOperated);
        scoreTotal = creditScoreService.calculate(scoreCriteria).intValue();       
    }

    @Then("^The credit score should be (-?\\d+)$")
    public void validateResult(int result) {
        Assert.assertThat(scoreTotal, Matchers.equalTo(result));
    }

}