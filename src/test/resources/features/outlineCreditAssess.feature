Feature: Credit Assessment Calculator
    As a user
    I want to get the credit assess socre
    
    Scenario Outline: Calculate Credit Assessment Score by <companyType> & No. of Employee=<numberOfEmployee> & Year Operated==<yearOperated>

        Given I have the Calculator
        When I pass <companyType> and <numberOfEmployee> and <yearOperated>
        Then The credit score should be <result>

        Examples:
        | companyType   | numberOfEmployee | yearOperated | result |
        | SOLE          | 0                | 0            | 12     |
        | SOLE          | 1                | 3            | 12     |
        | SOLE          | 10               | 3            | 32     |
        | SOLE          | 21               | 5            | 110    |
        | LTD           | 0                | 0            | 63     |
        | LTD           | 1                | 3            | 63     |
        | LTD           | 21               | 5            | 161    |
        | LTD           | 100              | 20           | 192    |        
        | PARTNERSHIP   | 0                | 0            | 75     |
        | PARTNERSHIP   | 1                | 3            | 75     |
        | PARTNERSHIP   | 20               | 10           | 166    |
        | PARTNERSHIP   | 100              | 20           | 204    |        
        | OTHERS        | 0                | 0            | 0      |        
        | OTHERS        | 1                | 3            | 0      |        
        | OTHERS        | 7                | 5            | 48     |        
        | OTHERS        | 100              | 20           | 129    |        
